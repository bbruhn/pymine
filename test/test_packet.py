"""
test packet
"""
import random
import uuid

import pytest

# we need to import types to have a complete registry
import pymine.packet as _
import pymine.registry.packet as pymine_reg_packet
import pymine.types as _


@pytest.mark.parametrize(
    "packet,connection_state,expected",
    [
        (
            b"\x00\xf2\x05\t127.0.0.1c\xdd\x02",
            "handshake",
            {
                "next_state": "login",
                "protocol_version": 754,
                "server_address": "127.0.0.1",
                "server_port": 25565,
                "identifier": 0,
            },
        ),
        (
            b"\x00\x10chickeaterbanana",
            "login",
            {"identifier": 0x00, "name": "chickeaterbanana"},
        ),
        (
            b"\x02\x1dL\x84\x1d!\xe3[\x97\xaenR(\x89\xac\x03\xa8\x10chickeaterbanana",
            "login",
            {
                "identifier": 0x02,
                "username": "chickeaterbanana",
                "uuid": uuid.UUID("1d4c841d-21e3-5b97-ae6e-522889ac03a8"),
            },
        ),
    ],
)
def test_packet_decode(packet, connection_state, expected):
    """
    test packet decode
    """
    result = pymine_reg_packet.decode_packet(connection_state, packet)
    assert result == expected


@pytest.mark.parametrize(
    "expected,connection_state,packet",
    [
        (
            b"\x00\xf2\x05\t127.0.0.1c\xdd\x02",
            "handshake",
            {
                "next_state": "login",
                "protocol_version": 754,
                "server_address": "127.0.0.1",
                "server_port": 25565,
                "identifier": 0,
            },
        ),
        (
            b"\x00\x10chickeaterbanana",
            "login",
            {"identifier": 0x00, "name": "chickeaterbanana"},
        ),
        (
            b"\x02\x1dL\x84\x1d!\xe3[\x97\xaenR(\x89\xac\x03\xa8\x10chickeaterbanana",
            "login",
            {
                "identifier": 0x02,
                "username": "chickeaterbanana",
                "uuid": uuid.UUID("1d4c841d-21e3-5b97-ae6e-522889ac03a8"),
            },
        ),
    ],
)
def test_packet_encode(packet, connection_state, expected):
    """
    test packet decode
    """
    varint = pymine_reg_packet.get_attribute_type_instance("varint")
    result = pymine_reg_packet.encode_packet(connection_state, packet)
    assert result == varint.encode(len(expected)) + expected


@pytest.mark.parametrize(
    "varint,expected,expected_length",
    [
        (0x00 .to_bytes(1, byteorder="big"), 0, 1),
        (0x01 .to_bytes(1, byteorder="big"), 1, 1),
        (0x02 .to_bytes(1, byteorder="big"), 2, 1),
        (0x10 .to_bytes(1, byteorder="big"), 16, 1),
        (0x7F .to_bytes(1, byteorder="big"), 127, 1),
        (0x7F19 .to_bytes(2, byteorder="big"), 127, 1),
        (0x8001 .to_bytes(2, byteorder="big"), 128, 2),
        (0x800156 .to_bytes(3, byteorder="big"), 128, 2),
        (0xFF01 .to_bytes(2, byteorder="big"), 255, 2),
        (0xFFFF7F .to_bytes(3, byteorder="big"), 2097151, 3),
        (0xFFFF7F78 .to_bytes(4, byteorder="big"), 2097151, 3),
        (0xFFFFFFFF07 .to_bytes(5, byteorder="big"), 2147483647, 5),
        (0xFFFFFFFF0F .to_bytes(5, byteorder="big"), -1, 5),
        (0x8080808008 .to_bytes(5, byteorder="big"), -2147483648, 5),
    ],
)
def test_decode_varint(varint, expected, expected_length):
    """
    test varint decode
    """
    value, length = pymine_reg_packet.get_attribute_type_instance(
        "varint"
    ).decode(varint)
    assert value == expected
    assert length == expected_length


@pytest.mark.parametrize(
    "varint,expected,expected_remaining",
    [
        (0x00 .to_bytes(1, byteorder="big"), 0, 1),
        (0x01 .to_bytes(1, byteorder="big"), 1, 1),
        (0x02 .to_bytes(1, byteorder="big"), 2, 1),
        (0x7F .to_bytes(1, byteorder="big"), 127, 1),
        (0x7F19 .to_bytes(2, byteorder="big"), 127, 1),
        (0x8001 .to_bytes(2, byteorder="big"), 128, 2),
        (0x800156 .to_bytes(3, byteorder="big"), 128, 2),
        (0xFF01 .to_bytes(2, byteorder="big"), 255, 2),
        (0xFFFF7F .to_bytes(3, byteorder="big"), 2097151, 3),
        (0xFFFF7F78 .to_bytes(4, byteorder="big"), 2097151, 3),
        (0xFFFFFFFF07 .to_bytes(5, byteorder="big"), 2147483647, 5),
        (
            0xFFFFFFFFFFFFFFFF7F .to_bytes(9, byteorder="big"),
            9223372036854775807,
            9,
        ),
        (0xFFFFFFFFFFFFFFFFFF01 .to_bytes(10, byteorder="big"), -1, 10),
        (
            0x80808080F8FFFFFFFF01 .to_bytes(10, byteorder="big"),
            -2147483648,
            10,
        ),
        (
            0x80808080808080808001 .to_bytes(10, byteorder="big"),
            -9223372036854775808,
            10,
        ),
    ],
)
def test_decode_varlong(varint, expected, expected_remaining):
    """
    test varlong decode
    """
    value, remaining_bytes = pymine_reg_packet.get_attribute_type_instance(
        "varlong"
    ).decode(varint)
    assert value == expected
    assert remaining_bytes == expected_remaining


@pytest.mark.parametrize(
    "value, bit_length",
    [
        (0, 32),
        (1, 32),
        (2, 32),
        (16, 32),
        (127, 32),
        (128, 32),
        (255, 32),
        (2097151, 32),
        (2147483647, 32),
        (-1, 32),
        (-8, 32),
        (-138, 32),
        (-2147483648, 32),
        (random.randint(-2147483648, 2147483647), 32),
        (0, 64),
        (1, 64),
        (2, 64),
        (127, 64),
        (128, 64),
        (-138, 64),
        (255, 64),
        (2097151, 64),
        (2147483647, 64),
        (-1, 64),
        (-8, 64),
        (-2147483648, 64),
        (9223372036854775807, 64),
        (-9223372036854775808, 64),
        (
            random.randint(-9223372036854775808, 9223372036854775807),
            64,
        ),
    ],
)
def test_encode_varint(value, bit_length):
    """
    test if encode varint works as expected
    """
    varint = pymine_reg_packet.get_attribute_type_instance(
        "varint", bit_length=bit_length
    )
    assert value == varint.decode(
        varint.encode(value),
    )[0]
