"""
test client module
"""
import asyncio
import unittest.mock as mock
import uuid

import pytest
import zmq

import pymine.client as pymine_client
import pymine.registry.client as pymine_reg_client
import pymine.types as pymine_types

# pylint: disable=no-self-use,unused-argument,too-few-public-methods,no-method-argument,missing-function-docstring,unsubscriptable-object,protected-access,undefined-variable


def test_client_db_memory():
    """
    test functionality of client_db
    """
    database = pymine_client.ClientDBMemory()
    client_id = uuid.uuid4()
    with pytest.raises(KeyError):
        database.get(client_id)

    with pytest.raises(KeyError):
        database.update_attributes(client_id)

    client = pymine_types.Client(
        client_id=client_id, address="13", state="handshake"
    )

    result = database.upsert(**client.dict())
    assert result.client_id == client_id
    assert result.state == "handshake"
    assert result.last_seen
    assert result.first_seen
    assert result.domain

    client.state = "play"
    result = database.upsert(**client.dict())
    assert result.state == "play"

    result = database.update_attributes(client_id, name="wow")
    assert result.name == "wow"

    assert result == database.get(client_id)


def test_process_client_request():
    """
    test process_client_request
    """
    test_client_id = uuid.uuid4()
    test_request = pymine_types.ClientRequest(
        type="whoop",
        client_id=test_client_id,
    )

    class TestClientSock:
        """
        mock client socket
        """

        async def recv_serialized(self, *args):
            """
            mocked recv
            """
            return test_request

        async def send_serialized(self, client, func):
            """
            mocked send
            """
            assert callable(func)
            assert client == "test_str"

    class TestClientDB:
        """
        mock client db
        """

        def get(self, client_id):
            """
            mocked get
            """
            assert client_id == test_client_id
            return "test_str"

    test_client_db = TestClientDB()

    def handler(request, client_db, pub_sock):
        assert client_db == test_client_db
        assert pub_sock is None
        assert request == test_request
        return test_client_id

    pymine_reg_client.register_client_handler(test_request.type)(handler)
    asyncio.run(
        pymine_client.process_client_request(
            TestClientSock(), None, test_client_db
        )
    )


def test_client_service_default_event():
    """
    client service
    """
    test_addresses = ["whoop", "foo", "bar"]
    test_publishers = ["bla", "bli", "blub"]

    class TestClientSocket:
        """
        spec for mock
        """

        def bind():
            pass

        async def poll():
            pass

        async def recv_serialized():
            pass

    async def poll(timeout):
        await asyncio.sleep(timeout / 1000.0)
        return zmq.POLLIN

    test_client_socket = mock.MagicMock(TestClientSocket)
    test_client_socket.poll = poll
    test_pub_socket = mock.MagicMock()

    class TestZMQContext:
        """
        mock zmq context
        """

        def socket(self, zmq_type):
            """
            return specified mock socket
            """
            if zmq_type == zmq.REP:
                return test_client_socket
            if zmq_type == zmq.PUB:
                return test_pub_socket
            raise AssertionError("unknown type")

    test_zmq_context = TestZMQContext()

    process_client_request = mock.AsyncMock(side_effect=RuntimeError)
    with mock.patch(
        "pymine.client.process_client_request", process_client_request
    ):
        with pytest.raises(RuntimeError):
            asyncio.run(
                pymine_client.client_service(
                    test_addresses,
                    test_publishers,
                    test_zmq_context,
                    poll_timeout=500,
                )
            )


def test_client_service():
    """
    client service
    """
    test_addresses = ["whoop", "foo", "bar"]
    test_publishers = ["bla", "bli", "blub"]

    class TestClientSocket:
        """
        spec for mock
        """

        def bind():
            pass

        async def poll():
            pass

        async def recv_serialized():
            pass

    async def poll(timeout):
        await asyncio.sleep(timeout / 1000.0)
        return zmq.POLLIN

    test_client_socket = mock.MagicMock(TestClientSocket)
    test_client_socket.poll = poll
    test_pub_socket = mock.MagicMock()

    class TestZMQContext:
        """
        mock zmq context
        """

        def socket(self, zmq_type):
            """
            return specified mock socket
            """
            if zmq_type == zmq.REP:
                return test_client_socket
            if zmq_type == zmq.PUB:
                return test_pub_socket
            raise AssertionError("unknown type")

    test_zmq_context = TestZMQContext()

    async def test_main():
        """
        async test main
        """
        test_event = asyncio.Event()
        task = asyncio.create_task(
            pymine_client.client_service(
                test_addresses,
                test_publishers,
                test_zmq_context,
                test_event,
                poll_timeout=500,
            )
        )
        await asyncio.sleep(0.5)
        test_event.set()
        await task

    process_client_request = mock.AsyncMock()
    with mock.patch(
        "pymine.client.process_client_request", process_client_request
    ):
        asyncio.run(test_main())

    process_client_request.assert_called()

    assert test_client_socket.bind.call_count == 3
    assert test_pub_socket.bind.call_count == 3

    assert test_client_socket.bind().__enter__.call_count == 3
    assert test_pub_socket.bind().__enter__.call_count == 3

    assert test_client_socket.bind().__exit__.call_count == 3
    assert test_pub_socket.bind().__exit__.call_count == 3


def test_client_handler_update_client_state():
    client_id = uuid.uuid4()
    client_db = pymine_client.ClientDBMemory()
    client_db.upsert(
        **pymine_types.Client(
            client_id=client_id,
            state="play",
            address="123",
        ).dict()
    )
    test_client = client_db.get(client_id)

    class TestPubSocket:
        """
        pub socket mock
        """

        def __init__(self):
            self.called_send_serialized = 0

        def send_serialized(self, objs, func):
            assert objs[0] == test_client.topic
            assert objs[1].client_id == test_client.client_id
            assert objs[1].state == "handshake"
            assert callable(func)
            self.called_send_serialized += 1

    test_pub_sock = TestPubSocket()

    test_request = pymine_types.ClientRequest(
        client_id=client_id,
        type="update_client_state",
        state="handshake",
    )
    _client_id = pymine_client._update_client_state(
        test_request, client_db, test_pub_sock
    )

    assert _client_id == client_id
    assert test_pub_sock.called_send_serialized == 1
    client_obj = client_db.get(client_id)
    assert client_obj.state == "handshake"


def test_client_handler_update_client_name():
    client_id = uuid.uuid4()
    client_db = pymine_client.ClientDBMemory()
    client_db.upsert(
        **pymine_types.Client(
            client_id=client_id,
            state="play",
            address="123",
        ).dict()
    )
    test_client = client_db.get(client_id)

    class TestPubSocket:
        """
        pub socket mock
        """

        def __init__(self):
            self.called_send_serialized = 0

        def send_serialized(self, objs, func):
            assert objs[0] == test_client.topic
            assert objs[1].client_id == test_client.client_id
            assert objs[1].name == "foo"
            assert callable(func)
            self.called_send_serialized += 1

    test_pub_sock = TestPubSocket()

    test_request = pymine_types.ClientRequest(
        client_id=client_id,
        type="update_client_name",
        name="foo",
    )
    _client_id = pymine_client._update_client_name(
        test_request, client_db, test_pub_sock
    )

    assert _client_id == client_id
    assert test_pub_sock.called_send_serialized == 0
    client_obj = client_db.get(client_id)
    assert client_obj.name == "foo"


def test_client_handler_disconnect_client():
    client_id = uuid.uuid4()
    client_db = pymine_client.ClientDBMemory()
    client_db.upsert(
        **pymine_types.Client(
            client_id=client_id,
            state="play",
            address="123",
        ).dict()
    )
    test_client = client_db.get(client_id)

    class TestPubSocket:
        """
        pub socket mock
        """

        def __init__(self):
            self.called_send_serialized = 0

        def send_serialized(self, objs, func):
            assert objs[0] == test_client.topic
            assert objs[1].client_id == test_client.client_id
            assert objs[1].state == "disconnect"
            assert callable(func)
            self.called_send_serialized += 1

    test_pub_sock = TestPubSocket()

    test_request = pymine_types.ClientRequest(
        client_id=client_id,
        type="disconnect_client",
    )
    _client_id = pymine_client._disconnect_client(
        test_request, client_db, test_pub_sock
    )

    assert _client_id == client_id
    assert test_pub_sock.called_send_serialized == 1
    client_obj = client_db.get(client_id)
    assert client_obj.state == "disconnect"


def test_client_handler_get_client():
    client_id = uuid.uuid4()
    client_db = pymine_client.ClientDBMemory()
    client_db.upsert(
        **pymine_types.Client(
            client_id=client_id,
            state="play",
            address="123",
        ).dict()
    )
    test_client = client_db.get(client_id)

    class TestPubSocket:
        """
        pub socket mock
        """

        def __init__(self):
            self.called_send_serialized = 0

        def send_serialized(self, objs, func):
            assert objs[0] == test_client.topic
            assert objs[1].client_id == test_client.client_id
            assert objs[1].state == "disconnect"
            assert callable(func)
            self.called_send_serialized += 1

    test_pub_sock = TestPubSocket()

    test_request = pymine_types.ClientRequest(
        client_id=client_id,
        type="get_client",
    )
    _client_id = pymine_client._get_client(
        test_request, client_db, test_pub_sock
    )

    assert _client_id == client_id
    assert test_pub_sock.called_send_serialized == 0


def test_client_handler_register_client_invalid():
    client_id = uuid.uuid4()
    client_db = pymine_client.ClientDBMemory()
    client_obj = pymine_types.Client(
        client_id=client_id,
        state="play",
        address="123",
    )

    test_request = pymine_types.ClientRequest(
        client_id=uuid.uuid4(),
        type="get_client",
        client=client_obj,
    )
    with pytest.raises(ValueError):
        _client_id = pymine_client._register_client(
            test_request, client_db, None
        )


def test_client_handler_register_client():
    client_id = uuid.uuid4()
    client_db = pymine_client.ClientDBMemory()
    client_obj = pymine_types.Client(
        client_id=client_id,
        state="play",
        address="123",
    )

    class TestPubSocket:
        """
        pub socket mock
        """

        def __init__(self):
            self.called_send_serialized = 0

        def send_serialized(self, objs, func):
            assert objs[0] == test_client.topic
            assert objs[1].client_id == test_client.client_id
            assert objs[1].state == "disconnect"
            assert callable(func)
            self.called_send_serialized += 1

    test_pub_sock = TestPubSocket()

    test_request = pymine_types.ClientRequest(
        client_id=client_id,
        type="get_client",
        client=client_obj,
    )
    _client_id = pymine_client._register_client(
        test_request, client_db, test_pub_sock
    )

    assert _client_id == client_id
    assert client_db.get(client_id)
    assert test_pub_sock.called_send_serialized == 0


@pytest.mark.asyncio
async def test_client_disconnect_client(unused_tcp_port_factory):
    client_address_rep = "tcp://127.0.0.1:{}".format(unused_tcp_port_factory())
    client_address_pub = "tcp://127.0.0.1:{}".format(unused_tcp_port_factory())
    client_db = pymine_client.ClientDBMemory()
    event = asyncio.Event()
    task = asyncio.create_task(
        pymine_client.client_service(
            [client_address_rep],
            [client_address_pub],
            event=event,
            poll_timeout=500,
            client_db=client_db,
        )
    )

    client_id = uuid.uuid4()
    client_obj = pymine_types.Client(
        client_id=client_id,
        state="handshake",
        address="123",
    )

    client_db.upsert(**client_obj.dict())
    client_sock = pymine_client.ClientSocket()
    with pytest.raises(RuntimeError):
        await client_sock.disconnect(client_id)

    with client_sock.connect(client_address_rep):
        await client_sock.disconnect(client_id)
    event.set()
    await task
    assert client_db.get(client_id).state == "disconnect"


@pytest.mark.asyncio
async def test_client_register_client(unused_tcp_port_factory):
    client_address_rep = "tcp://127.0.0.1:{}".format(unused_tcp_port_factory())
    client_address_pub = "tcp://127.0.0.1:{}".format(unused_tcp_port_factory())
    client_db = pymine_client.ClientDBMemory()
    event = asyncio.Event()
    task = asyncio.create_task(
        pymine_client.client_service(
            [client_address_rep],
            [client_address_pub],
            event=event,
            poll_timeout=500,
            client_db=client_db,
        )
    )

    client_id = uuid.uuid4()
    client_obj = pymine_types.Client(
        client_id=client_id,
        state="handshake",
        address="123",
    )

    client_sock = pymine_client.ClientSocket()
    with pytest.raises(RuntimeError):
        await client_sock.register(client_obj)

    with client_sock.connect(client_address_rep):
        await client_sock.register(client_obj)
    event.set()
    await task
    assert client_db.get(client_id).state == "handshake"


@pytest.mark.asyncio
async def test_client_get_client(unused_tcp_port_factory):
    client_address_rep = "tcp://127.0.0.1:{}".format(unused_tcp_port_factory())
    client_address_pub = "tcp://127.0.0.1:{}".format(unused_tcp_port_factory())
    client_db = pymine_client.ClientDBMemory()
    event = asyncio.Event()
    task = asyncio.create_task(
        pymine_client.client_service(
            [client_address_rep],
            [client_address_pub],
            event=event,
            poll_timeout=500,
            client_db=client_db,
        )
    )

    client_id = uuid.uuid4()
    client_obj = pymine_types.Client(
        client_id=client_id,
        state="handshake",
        address="123",
    )

    client_db.upsert(**client_obj.dict())
    client_sock = pymine_client.ClientSocket()
    with pytest.raises(RuntimeError):
        await client_sock.get(client_id)

    with client_sock.connect(client_address_rep):
        response = await client_sock.get(client_id)
    event.set()
    await task
    assert client_db.get(client_id) == response


@pytest.mark.asyncio
async def test_client_update_name(unused_tcp_port_factory):
    client_address_rep = "tcp://127.0.0.1:{}".format(unused_tcp_port_factory())
    client_address_pub = "tcp://127.0.0.1:{}".format(unused_tcp_port_factory())
    client_db = pymine_client.ClientDBMemory()
    event = asyncio.Event()
    task = asyncio.create_task(
        pymine_client.client_service(
            [client_address_rep],
            [client_address_pub],
            event=event,
            poll_timeout=500,
            client_db=client_db,
        )
    )

    client_id = uuid.uuid4()
    client_obj = pymine_types.Client(
        client_id=client_id,
        state="handshake",
        address="123",
    )

    client_db.upsert(**client_obj.dict())
    client_sock = pymine_client.ClientSocket()
    with pytest.raises(RuntimeError):
        await client_sock.update_name(client_id, "foo")

    with client_sock.connect(client_address_rep):
        response = await client_sock.update_name(client_id, "foo")
    event.set()
    await task
    assert client_db.get(client_id) == response
    assert client_db.get(client_id).name == "foo"


@pytest.mark.asyncio
async def test_client_update_state(unused_tcp_port_factory):
    client_address_rep = "tcp://127.0.0.1:{}".format(unused_tcp_port_factory())
    client_address_pub = "tcp://127.0.0.1:{}".format(unused_tcp_port_factory())
    client_db = pymine_client.ClientDBMemory()
    event = asyncio.Event()
    task = asyncio.create_task(
        pymine_client.client_service(
            [client_address_rep],
            [client_address_pub],
            event=event,
            poll_timeout=500,
            client_db=client_db,
        )
    )

    client_id = uuid.uuid4()
    client_obj = pymine_types.Client(
        client_id=client_id,
        state="handshake",
        address="123",
    )

    client_db.upsert(**client_obj.dict())
    client_sock = pymine_client.ClientSocket()
    with pytest.raises(RuntimeError):
        await client_sock.update_state(client_id, "play")

    with client_sock.connect(client_address_rep):
        response = await client_sock.update_state(client_id, "play")
    event.set()
    await task
    assert client_db.get(client_id) == response
    assert client_db.get(client_id).state == "play"
