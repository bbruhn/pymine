"""
initialize some fixtures and tests
"""
import asyncio

import faker
import faker.providers
import pytest

import pymine.client as pymine_client
import pymine.types as pymine_types


@pytest.fixture
def pymine_client_db():
    """
    initialize a client db memory
    """
    return pymine_client.ClientDBMemory()


class PymineFaker(faker.providers.BaseProvider):
    """
    pymine faker provider
    """

    def pymine_client(self, **kwargs):
        """
        generate a pymine client
        """
        default_options = {
            "client_id": self.generator.uuid4(),
            "address": self.generator.ipv4(),
            "state": self.random_element(
                ["handshake", "login", "play", "disconnect"]
            ),
            "domain": "client.connection.pymine.de",
            "name": self.generator.word(),
            "first_seen": self.generator.past_datetime(
                tzinfo=self.generator.pytimezone()
            ),
            "last_seen": self.generator.past_datetime(
                tzinfo=self.generator.pytimezone()
            ),
        }
        default_options.update(kwargs)
        return pymine_types.Client(**default_options)


@pytest.fixture
def pymine_faker():
    """
    pymine faker
    """
    faker_obj = faker.Faker()
    faker_obj.add_provider(PymineFaker)
    return faker_obj


@pytest.fixture
async def pymine_client_service(
    unused_tcp_port_factory, pymine_client_db
):  # pylint: disable=redefined-outer-name
    """
    fixture for client_service
    """
    client_address_rep = "tcp://127.0.0.1:{}".format(unused_tcp_port_factory())
    client_address_pub = "tcp://127.0.0.1:{}".format(unused_tcp_port_factory())
    event = asyncio.Event()
    task = asyncio.create_task(
        pymine_client.client_service(
            [client_address_rep],
            [client_address_pub],
            event=event,
            poll_timeout=500,
            client_db=pymine_client_db,
        )
    )
    client_sock = pymine_client.ClientSocket()
    with client_sock.connect(client_address_rep):
        yield {
            "client_sock": client_sock,
            "pub_sock_address": client_address_pub,
        }
    event.set()
    await task
