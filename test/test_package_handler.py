"""
test package handler module
"""
import pytest

import pymine.package_handler as pymine_package_handler
import pymine.types as pymine_types

# pylint: disable=no-self-use,unused-argument,too-few-public-methods,no-method-argument,missing-function-docstring,unsubscriptable-object,protected-access,undefined-variable


@pytest.mark.asyncio
async def test_handle_0x00_handshake(pymine_client_service, pymine_faker):
    """
    test handle 0x00 handshake
    """
    client_obj = pymine_faker.pymine_client(state="handshake")
    await pymine_client_service["client_sock"].register(client_obj)
    test_packet = pymine_types.Packet(
        client_id=client_obj.client_id,
        client_state=client_obj.state,
        packet={"next_state": "play"},
    )
    await pymine_package_handler.handle_0x00_handshake(
        test_packet,
        None,
        pymine_client_service["client_sock"],
    )

    client_obj = await pymine_client_service["client_sock"].get(
        client_obj.client_id
    )
    assert client_obj.state == "play"


@pytest.mark.asyncio
async def test_handle_0x00_login(pymine_client_service, pymine_faker):
    """
    test handle 0x00 handshake
    """
    client_obj = pymine_faker.pymine_client(name="old_name")
    await pymine_client_service["client_sock"].register(client_obj)
    test_packet = pymine_types.Packet(
        client_id=client_obj.client_id,
        client_state=client_obj.state,
        packet={"name": "new_name"},
    )

    class TestPubSocket:
        """
        mock test pub socket
        """

        def __init__(self):
            self.call_count = 0

        def send_serialized(self, objs, func):
            """
            test send serialized
            """
            assert callable(func)
            assert objs[0] == client_obj.state_topic
            assert objs[1].client_id == client_obj.client_id
            self.call_count += 1

    test_pub_sock = TestPubSocket()
    await pymine_package_handler.handle_0x00_login(
        test_packet,
        test_pub_sock,
        pymine_client_service["client_sock"],
    )

    client_obj = await pymine_client_service["client_sock"].get(
        client_obj.client_id
    )
    assert client_obj.name == "new_name"
    assert test_pub_sock.call_count == 1
