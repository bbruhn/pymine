# PyMine
pymine is a minecraft server written in python.

## Development Setup
To start developing on pymine you have to install poetry. Afterwards you can
install the development setup.

    poetry install

To be able to run the documentation or the tests you have to install the
extras `doc` and `test`.

    poetry install -E doc -E test

To run the tests run pytest.

    poetry run pytest

To run the server components:

    poetry run pymine --debug server package-handler
    poetry run pymine --debug server client
    poetry run pymine --debug server connection

or with podman:

    podman run -p "25566:25566" -p "25567:25567" -d --rm registry.gitlab.com/bbruhn/pymine --debug server package-handler
    podman run -p "25569:25569" -p "25568:25568" -d --rm registry.gitlab.com/bbruhn/pymine --debug server client
    podman run -p "25565:25565" -d --rm registry.gitlab.com/bbruhn/pymine --debug server connection

## Useful links

* https://wiki.vg/Main_Page
