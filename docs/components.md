# Components
pymine has a set of components which are started seperately from each other.

## Structure

```mermaid
graph
Client(Client) -- connects --- ConnectionService
ConnectionService -- push --> PackageHandler
PackageHandler -- publish --> ConnectionService
PackageHandler -- requests --> PlayerService --> PlayerDB[(PlayerDB)]
PackageHandler -- requests --> WorldService --> WorldDB[(WorldDB)]
PlayerService -- publish --> ConnectionService
WorldService -- publish --> ConnectionService
```
