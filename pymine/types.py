"""
define pydantic types
"""
import ctypes
import datetime as dt
import enum
import itertools as it
import os
import typing
import uuid

import pydantic

from .registry import packet as pymine_reg_packet


class ZMQBaseModel(pydantic.BaseModel):
    """
    base methods for a zmq model
    """

    @classmethod
    def zmq_deserialize(cls, frames):
        """
        deserialize zmq message
        """
        if len(frames) == 2:
            result = []
            result.append(frames[0].decode())
            msg = cls.parse_raw(frames[1])
            if hasattr(msg, "_zmq_deserialize"):
                msg = (
                    msg._zmq_deserialize()  # pylint: disable=protected-access
                )
            result.append(msg)
            return result

        obj = cls.parse_raw(frames[0])
        if hasattr(obj, "_zmq_deserialize"):
            obj = obj._zmq_deserialize()  # pylint: disable=protected-access
        return obj

    @classmethod
    def zmq_serialize(cls, obj):
        """
        serialize zmq message
        """
        result = []
        if isinstance(obj, list):
            for msg in obj:
                if isinstance(msg, str):
                    result.append(msg.encode())
                elif isinstance(msg, cls):
                    if hasattr(msg, "_zmq_serialize"):
                        msg = (
                            msg._zmq_serialize()  # pylint: disable=protected-access
                        )
                    result.append(msg.json().encode())
                else:
                    raise TypeError("invalid type")
        elif not isinstance(obj, cls):
            raise TypeError("invalid type")
        elif isinstance(obj, cls):
            if hasattr(obj, "_zmq_serialize"):
                obj = obj._zmq_serialize()  # pylint: disable=protected-access
            result.append(obj.json().encode())
        return result


class ClientState(str, enum.Enum):
    """
    defines client states
    """

    HANDSHAKE = "handshake"
    LOGIN = "login"
    PLAY = "play"
    DISCONNECT = "disconnect"


class Client(ZMQBaseModel):
    """
    defines client data object
    """

    client_id: uuid.UUID
    address: str
    state: ClientState
    domain: typing.Optional[str]
    name: typing.Optional[str]
    first_seen: typing.Optional[dt.datetime]
    last_seen: typing.Optional[dt.datetime]

    @property
    def topic(self):
        """
        generate subscription topic independent from state
        """
        return ".".join(
            it.chain(
                reversed(self.domain.split(".")),
                [str(self.client_id)],
            )
        )

    @property
    def state_topic(self):
        """
        generate subscription topic dependent from state
        """
        return ".".join(
            it.chain(
                reversed(self.domain.split(".")),
                [str(self.state), str(self.client_id)],
            )
        )


class ClientRequest(ZMQBaseModel):
    """
    client request obj
    """

    type: str
    client_id: uuid.UUID
    client: typing.Optional[Client]
    state: typing.Optional[str]
    name: typing.Optional[str]


class Packet(ZMQBaseModel):
    """
    UncompressedPacket
    """

    client_id: uuid.UUID
    client_state: ClientState
    packet: dict

    def _zmq_deserialize(self):
        """
        deserialize zmq message
        """
        self.packet = pymine_reg_packet.decode_packet_json(
            self.client_state, self.packet
        )
        return self

    def _zmq_serialize(self):
        """
        serialize zmq message
        """
        self.packet = pymine_reg_packet.encode_packet_json(
            self.client_state, self.packet
        )
        return self


@pymine_reg_packet.register_packet_attribute_type("varint")
class Varint:
    """
    Varint type
    """

    def __init__(
        self,
        bit_length=32,
        minimum=None,
        maximum=None,
    ):
        if bit_length not in (32, 64):
            raise ValueError("invalid bit_length")
        self._bit_length = bit_length
        self._minimum = minimum
        self._maximum = maximum

    def decode(self, obj):
        """
        decode a varint
        """
        idx = 0
        result = 0
        for idx, byte in enumerate(obj):
            if idx >= 5 and self._bit_length == 32:
                raise RuntimeError("VarInt is too big")
            if idx >= 10 and self._bit_length == 64:
                raise RuntimeError("VarLong is too big")
            value = byte & 0b01111111
            result = result | (value << (7 * idx))

            if byte & 0b10000000 == 0:
                break
        else:
            # We finished without finding the stop byte so its incomplete!
            raise ValueError("varint is incomplete we need more data")

        if self._bit_length == 32:
            result = ctypes.c_int32(result).value
        else:
            result = ctypes.c_int64(result).value

        return result, idx + 1

    def encode(self, value):
        """
        encode a varint
        """
        if (
            self._minimum is not None
            and self._maximum is not None
            and not (self._minimum <= value < self._maximum)
        ):
            raise ValueError("value have to be in range")
        result = b""
        if value == 0:
            return value.to_bytes(1, byteorder="big")
        value = int.from_bytes(
            value.to_bytes(
                self._bit_length // 8, byteorder="big", signed=True
            ),
            byteorder="big",
            signed=False,
        )
        while value != 0:
            byte = value
            if value.bit_length() > 8:
                byte = value & int("0" * (value.bit_length() - 7) + "1" * 7, 2)
            temp = byte & 0b01111111
            value = value >> 7
            if value != 0:
                temp |= 0b10000000
            result += temp.to_bytes(1, byteorder="big")
        if self._bit_length == 32 and len(result) > 5:
            raise ValueError("value is too big")
        if self._bit_length == 64 and len(result) > 10:
            raise ValueError("value is too big")
        return result


@pymine_reg_packet.register_packet_attribute_type("varlong")
class Varlong(Varint):
    """
    varlong
    """

    def __init__(self, minimum=None, maximum=None):
        super().__init__(bit_length=64, minimum=minimum, maximum=maximum)


@pymine_reg_packet.register_packet_attribute_type("string")
class String:
    """
    definition of string type
    """

    def __init__(self, max_length=32767):
        self._max_length = max_length

    def decode(self, data):
        """
        decode a string with max length
        """
        length, length_read = Varint().decode(data)
        if length > self._max_length * 4:
            raise RuntimeError("string was too big")
        string = data[length_read : length_read + length]
        return string.decode("utf-8"), length_read + length

    def encode(self, string):
        """
        encode string to wire format
        """
        string = str(string)
        if len(string) > self._max_length:
            raise RuntimeError("string was too big")
        data = string.encode("utf-8")
        if len(data) > self._max_length * 4:
            raise RuntimeError("string was too big")
        length = Varint().encode(len(data))
        return length + data


@pymine_reg_packet.register_packet_attribute_type("int")
@pymine_reg_packet.register_packet_attribute_type("integer")
class Integer:
    """
    definition of integer type
    """

    def __init__(self, bit_length, signed):
        self._signed = signed
        self._bit_length = bit_length

    def decode(self, data):
        """
        decode a int
        """
        return (
            int.from_bytes(
                data[: self._bit_length // 8],
                byteorder="big",
                signed=self._signed,
            ),
            self._bit_length // 8,
        )

    def encode(self, integer):
        """
        encode integer with size of bit to wire format
        """
        return integer.to_bytes(
            self._bit_length // 8, byteorder="big", signed=self._signed
        )


@pymine_reg_packet.register_packet_attribute_type("uint16")
@pymine_reg_packet.register_packet_attribute_type("unsigned integer 16")
class UnsignedInteger16(Integer):
    """
    definition of unsigned integer 16
    """

    def __init__(self):
        super().__init__(16, False)


@pymine_reg_packet.register_packet_attribute_type("int8")
@pymine_reg_packet.register_packet_attribute_type("integer 8")
class Integer8(Integer):
    """
    definition of integer 8
    """

    def __init__(self):
        super().__init__(8, True)


@pymine_reg_packet.register_packet_attribute_type("uint8")
@pymine_reg_packet.register_packet_attribute_type("unsigned integer 8")
class UnsignedInteger8(Integer):
    """
    definition of integer 8
    """

    def __init__(self):
        super().__init__(8, False)


@pymine_reg_packet.register_packet_attribute_type("int32")
@pymine_reg_packet.register_packet_attribute_type("integer 32")
class Integer32(Integer):
    """
    definition of Integer 32
    """

    def __init__(self):
        super().__init__(32, False)


@pymine_reg_packet.register_packet_attribute_type("int64")
@pymine_reg_packet.register_packet_attribute_type("integer 64")
class Integer64(Integer):
    """
    definition of Integer 64
    """

    def __init__(self):
        super().__init__(64, False)


@pymine_reg_packet.register_packet_attribute_type(
    "boolean", func_type="encode"
)
def encode_boolean(data):
    """
    decode a boolean
    """
    value, read_length = UnsignedInteger8().decode(data)
    if value == 0x01:
        return True, read_length

    if value == 0x00:
        return False, read_length

    raise RuntimeError(f"invalid boolean {value}")


@pymine_reg_packet.register_packet_attribute_type(
    "boolean", func_type="decode"
)
def decode_boolean(boolean):
    """
    encode boolean to wire format
    """
    integer = UnsignedInteger8()
    if boolean:
        return integer.encode(0x01)
    return integer.encode(0x00)


@pymine_reg_packet.register_packet_attribute_type("bitmask")
class Bitmask:
    """
    definition of bitmask
    """

    def __init__(self, type_def, values):
        self._type_def = type_def
        self._bit_values = values
        self._values_bit = {v: k for k, v in values.items()}

    def decode(self, data):
        """
        decode bitmask
        """
        bitvalue, read_length = self._type_def.decode(data)
        result = []
        for bit, value in self._bit_values.items():
            if bit & bitvalue >= 0:
                result.append(value)
        return result, read_length

    def encode(self, data):
        """
        encode bitmask
        """
        result = 0
        for val in data:
            result |= self._values_bit[val]
        return self._type_def.encode(result)


@pymine_reg_packet.register_packet_attribute_type("raw")
class RawData:
    """
    definition of raw data
    """

    def decode(self, data):  # pylint: disable=no-self-use
        """
        decode raw data
        """
        return data, len(data)

    def encode(self, data):  # pylint: disable=no-self-use
        """
        encode raw data
        """
        return data


@pymine_reg_packet.register_packet_attribute_type("array")
@pymine_reg_packet.register_packet_attribute_type("list")
class Array:
    """
    definition of array
    """

    def __init__(self, inner_type, length_type):
        self._inner_type = inner_type
        self._length_type = length_type

    def decode(self, data):
        """
        decode array to wire format
        """
        array_length, length = self._length_type.decode(data)
        result = []
        for _ in range(array_length):
            val, _length = self._inner_type.decode(data[length:])
            result.append(val)
            length += _length
        return result, length

    def encode(self, array):
        """
        encode array to wire format
        """
        result = self._length_type.encode(len(array))
        for obj in array:
            result += self._inner_type.encode(obj)
        return result


@pymine_reg_packet.register_packet_attribute_type(
    "uuid", func_type="encode_json"
)
def encode_uuid_json(data):
    """
    encode uuid for json
    """
    return data.hex


@pymine_reg_packet.register_packet_attribute_type("uuid", func_type="decode")
def decode_uuid(data):
    """
    decode uuid from wire format
    """
    val, length = Integer(128, False).decode(data)
    return uuid.UUID(int=val), length


@pymine_reg_packet.register_packet_attribute_type(
    "uuid", func_type="decode_json"
)
def decode_uuid_json(data):
    """
    decode uuid for json
    """
    return uuid.UUID(hex=data)


@pymine_reg_packet.register_packet_attribute_type("uuid", func_type="encode")
def encode_uuid(uuid_val):
    """
    encode uuid from string to wire format
    """
    return Integer(128, False).encode(uuid_val.int)


@pymine_reg_packet.register_packet_attribute_type("dimension codec")
class DimensionCodec:
    """
    definition of Dimension Codec
    """

    def __init__(self):
        with open(os.path.dirname(__file__) + "/data/test2.nbt", "rb") as fobj:
            self._result = fobj.read()

    def encode(self, _):
        """
        encode DimensionCodec
        """
        return self._result

    def decode(self, data):
        """
        decode Dimension Codec
        """
        raise NotImplementedError()


@pymine_reg_packet.register_packet_attribute_type("dimension")
class Dimension:
    """
    definition of Dimension
    """

    def __init__(self):
        self._result = b""

    def encode(self, _):
        """
        encode Dimension
        """
        return self._result

    def decode(self, _):
        """
        decode Dimension
        """
        raise NotImplementedError()


@pymine_reg_packet.register_packet_attribute_type("enum")
class Enum:
    """
    definition of Enum
    """

    def __init__(self, type_def, values, kwargs=None):
        self._type_def = type_def
        self._kwargs = kwargs or {}
        self._values = values
        self._reverse_map = {v: k for k, v in self._values.items()}

    @property
    def type_def(self):
        """
        return type def instance
        """
        return pymine_reg_packet.get_attribute_type_instance(
            self._type_def, **self._kwargs
        )

    def decode(self, data):
        """
        decode a enum and return enum value
        """
        value, read_length = self.type_def.decode(data)
        return self._values[value], read_length

    def encode(self, data):
        """
        encode a enum
        """
        return self.type_def.encode(self._reverse_map[data])
