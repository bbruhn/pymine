"""
package handler
"""
import asyncio
import contextlib as ctx
import logging

import zmq
import zmq.asyncio

from . import client as pymine_client
from . import types as pymine_types
from .registry import packet as pymine_reg_packet

_log = logging.getLogger(__name__)


async def package_handler(
    pull_addresses, pub_addresses, client_address, zmq_context=None
):
    """
    starts a pull socket and waits for packages to handle
    """
    if not zmq_context:
        zmq_context = zmq.asyncio.Context()

    pull_sock = zmq_context.socket(zmq.PULL)
    pub_sock = zmq_context.socket(zmq.PUB)
    client_sock = pymine_client.ClientSocket(zmq_context)

    with ctx.ExitStack() as zmq_socks:
        zmq_socks.enter_context(client_sock.connect(client_address))
        for address in pull_addresses:
            zmq_socks.enter_context(pull_sock.bind(address))

        for address in pub_addresses:
            zmq_socks.enter_context(pub_sock.bind(address))

        while True:
            if await pull_sock.poll() == 0:
                await asyncio.sleep(0.1)
            msg = await pull_sock.recv_serialized(
                pymine_types.Packet.zmq_deserialize
            )
            _log.debug("package handler received: %r", msg)

            handler_result = pymine_reg_packet.get_packet_handler(
                msg.packet["identifier"], msg.client_state
            )(msg, pub_sock, client_sock)

            if asyncio.iscoroutine(handler_result):
                handler_result = await handler_result

            if handler_result:
                _log.debug("has handler result: %r", handler_result)


@pymine_reg_packet.register_packet_handler(0x00, "handshake")
async def handle_0x00_handshake(packet, _, client_sock):
    """
    handle packet 0x00 in client state handshake
    """
    _log.debug(
        "handle handshake: %s (%r)",
        packet.client_id,
        packet.packet["next_state"],
    )
    await client_sock.update_state(
        packet.client_id, packet.packet["next_state"]
    )


@pymine_reg_packet.register_packet_handler(0x00, "login")
async def handle_0x00_login(packet, pub_sock, client_sock):
    """
    handle login
    """
    _log.debug(
        "handle login: %s (%r)", packet.client_id, packet.packet["name"]
    )
    client_obj = await client_sock.update_name(
        packet.client_id, packet.packet["name"]
    )
    packet = pymine_types.Packet(
        client_id=client_obj.client_id,
        client_state=client_obj.state,
        packet={
            "identifier": 0x02,
            "username": client_obj.name,
            "uuid": client_obj.client_id,
        },
    )
    pub_sock.send_serialized(
        [client_obj.state_topic, packet], pymine_types.Packet.zmq_serialize
    )
