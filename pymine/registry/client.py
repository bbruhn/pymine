"""
pymine client registry
"""
_CLIENT_HANDLER = {}


def register_client_handler(request_type):
    """
    register client handler
    """

    def _inner(func):
        _CLIENT_HANDLER[request_type] = func
        return func

    return _inner


def get_client_handler(request_type):
    """
    get client handler
    """
    return _CLIENT_HANDLER[request_type]
