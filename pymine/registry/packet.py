"""
pymine packet registry
"""
import collections as col

# Packet Registry

_PACKET_REGISTRY = {}

_PACKET_ATTRIBUTE_TYPES = {}


def register_packet(packet_identifier, connection_state, attributes):
    """
    register packet in registry
    """
    packet_result = attributes
    if not isinstance(attributes, ObjectPacketAttribute):
        packet_result = ObjectPacketAttribute(
            f"{packet_identifier!r}_{connection_state!r}", attributes
        )

    _PACKET_REGISTRY[packet_identifier, connection_state] = packet_result


def decode_packet(connection_state, raw_obj):
    """
    decode packet from bytes and connection_state
    """
    identifier, parsed_size = get_attribute_type_instance("varint").decode(
        raw_obj
    )
    result = {
        "identifier": identifier,
    }

    try:
        packet_object = _PACKET_REGISTRY[identifier, connection_state]
    except KeyError as ex:
        raise NotImplementedError(
            f"o.o packet isn't implemented: {raw_obj!r}"
        ) from ex

    obj_result, obj_parsed_size = packet_object.decode(raw_obj[parsed_size:])
    parsed_size += obj_parsed_size
    result.update(obj_result)
    if parsed_size != len(raw_obj):
        raise ValueError("couldn't parse the whole packet")
    return result


def encode_packet(connection_state, obj):
    """
    encode packet from obj and connection_state
    """
    identifier = obj["identifier"]
    result = b""
    varint = get_attribute_type_instance("varint")

    result += varint.encode(identifier)

    try:
        packet_object = _PACKET_REGISTRY[identifier, connection_state]
    except KeyError as ex:
        raise NotImplementedError(
            f"o.o packet isn't implemented: {obj!r}"
        ) from ex
    result += packet_object.encode(obj)
    return varint.encode(len(result)) + result


def encode_packet_json(connection_state, obj):
    """
    encode packet to json
    """
    identifier = obj["identifier"]
    try:
        packet_obj = _PACKET_REGISTRY[identifier, connection_state]
    except KeyError as ex:
        raise NotImplementedError(
            f"o.o packet isn't implemented: {obj!r}"
        ) from ex
    result = {
        "identifier": identifier,
    }
    obj_result = packet_obj.encode_json(obj)
    result.update(obj_result)
    return result


def decode_packet_json(connection_state, obj):
    """
    decode packet from json
    """
    identifier = obj["identifier"]

    try:
        packet_obj = _PACKET_REGISTRY[identifier, connection_state]
    except KeyError as ex:
        raise NotImplementedError(
            f"o.o packet isn't implemented: {obj!r}"
        ) from ex

    obj_result = packet_obj.decode_json(obj)
    obj.update(obj_result)

    return obj


class PacketAttribute:
    """
    defines a packet attribute
    """

    def __init__(self, name, attr_type, kwargs=None, optional=False):
        self.name = name
        self.optional = optional
        self._attribute_type_name = attr_type
        self._attr_kwargs = kwargs
        self._type = None

    @property
    def _attribute_type(self):
        """
        get attribute type from registry
        """
        if not self._type:
            self._type = get_attribute_type(self._attribute_type_name)
            if isinstance(self._type, type):
                self._type = self._type(**self._kwargs)
        return self._type

    @property
    def _kwargs(self):
        """
        return kwargs
        """
        if self._attr_kwargs:
            return self._attr_kwargs
        return {}

    def encode(self, obj):
        """
        encode attribute
        """
        return self._attribute_type.encode(obj)

    def decode(self, raw_obj):
        """
        decode attribute
        """
        return self._attribute_type.decode(raw_obj)

    def decode_json(self, obj):  # pylint: disable=no-self-use
        """
        decode json encoded attribute
        """
        return obj

    def encode_json(self, obj):  # pylint: disable=no-self-use
        """
        encode json encoded attribute
        """
        return obj


class ObjectPacketAttribute(PacketAttribute):
    """
    represents a object as packet attribute
    """

    def __init__(
        self, name, attributes, optional=False
    ):  # pylint: disable=super-init-not-called
        self.name = name
        self.optional = optional
        object_attributes = []
        for attribute in attributes:
            if isinstance(attribute, PacketAttribute):
                object_attributes.append(attribute)
            elif isinstance(attribute, dict):
                try:
                    object_attributes.append(PacketAttribute(**attribute))
                except TypeError as ex:
                    raise ValueError(f"invalid attribute: {attribute}") from ex
            else:
                try:
                    object_attributes.append(PacketAttribute(*attribute))
                except TypeError as ex:
                    raise ValueError(f"invalid attribute: {attribute}") from ex
        self._attributes = object_attributes

    def encode(self, obj):
        result = b""
        for attribute in self._attributes:
            result += attribute.encode(obj[attribute.name])
        return result

    def decode(self, raw_obj):
        parsed_size = 0
        result = {}
        for attribute in self._attributes:
            value, size = attribute.decode(raw_obj[parsed_size:])
            parsed_size += size
            result[attribute.name] = value
        return result, parsed_size

    def _json_handler(self, obj, operation):
        """
        helper function for decode and encode json object
        """
        result = {}
        for attribute in self._attributes:
            value = obj[attribute.name]
            if hasattr(attribute.attribute_type, operation):
                value = attribute.attribute_type.decode_json(
                    obj[attribute.name]
                )
            result[attribute.name] = value

        return result

    def decode_json(self, obj):
        return self._json_handler(obj, "decode_json")

    def encode_json(self, obj):
        return self._json_handler(obj, "encode_json")


def get_attribute_type_instance(name, **kwargs):
    """
    get attribute type from registry
    """
    _type = get_attribute_type(name)
    if isinstance(_type, type):
        _type = _type(**kwargs)
    return _type


def get_attribute_type(name):
    """
    get attribute type from registry
    """
    if name not in _PACKET_ATTRIBUTE_TYPES:
        raise ValueError(f"unknown attribute type {name}")
    return _PACKET_ATTRIBUTE_TYPES[name]


def register_packet_attribute_type(name, func_type=None):
    """
    register a packet attribute type
    """

    def _inner(obj):
        """
        inner function
        """
        if not func_type and hasattr(obj, "decode") and hasattr(obj, "encode"):
            _PACKET_ATTRIBUTE_TYPES[name] = obj
            return obj
        if func_type and callable(obj):
            encode = None
            decode = None
            encode_json = None
            decode_json = None

            if name in _PACKET_ATTRIBUTE_TYPES:
                encode = _PACKET_ATTRIBUTE_TYPES[name].encode
                decode = _PACKET_ATTRIBUTE_TYPES[name].decode

                if hasattr(_PACKET_ATTRIBUTE_TYPES[name], "decode_json"):
                    decode_json = _PACKET_ATTRIBUTE_TYPES[name].decode_json

                if hasattr(_PACKET_ATTRIBUTE_TYPES[name], "encode_json"):
                    encode_json = _PACKET_ATTRIBUTE_TYPES[name].encode_json

            if func_type == "encode":
                encode = obj
            elif func_type == "decode":
                decode = obj
            elif func_type == "encode_json":
                encode_json = obj
            elif func_type == "decode_json":
                decode_json = obj
            else:
                raise ValueError(f"unknown func_type: {func_type}")

            _PACKET_ATTRIBUTE_TYPES[name] = _AbstractAttributeType(
                decode=decode,
                encode=encode,
                encode_json=encode_json,
                decode_json=decode_json,
            )
            return obj
        raise ValueError("type isn't supportet")

    return _inner


class _AbstractAttributeType:
    DecodeResult = col.namedtuple("DecodeResult", ["value", "size"])

    def __init__(
        self, encode=None, decode=None, decode_json=None, encode_json=None
    ):
        self._encode = encode
        self._decode = decode
        if decode_json:
            self.decode_json = decode_json
        if encode_json:
            self.encode_json = encode_json

    def encode(self, obj):
        """
        encode python object to bytes
        """
        if self._encode:
            return self._encode(obj)
        raise TypeError("no encode for type registered")

    def decode(self, raw_obj):
        """
        decode bytes to python object
        """
        if self._decode:
            return self.DecodeResult(*self._decode(raw_obj))
        raise TypeError("no decode for type registered")


# packet handler
_PACKET_HANDLER = {}


def register_packet_handler(identifier, connection_state):
    """
    register packet handler
    """

    def _inner(func):
        _PACKET_HANDLER[identifier, connection_state] = func
        return func

    return _inner


def get_packet_handler(identifier, connection_state):
    """
    get a packet handler
    """
    return _PACKET_HANDLER[identifier, connection_state]
