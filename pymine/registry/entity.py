"""
entity module
"""
import contextlib as ctx
import logging

_log = logging.getLogger(__name__)


def create_entity_registry(_):
    """
    closure to generate entity registries
    """
    entities = {0: None}

    @ctx.contextmanager
    def entity_registry(entity_id):
        if entity_id is None:
            entity_id = max(entities.keys()) + 1
            entities[entity_id] = _create_entity(entity_id)
        yield entities[entity_id]

    return entity_registry


def _create_entity(entity_id):
    """
    creates an entity
    """
    return {
        "eid": entity_id,
    }
