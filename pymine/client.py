"""
client module
"""
import asyncio
import contextlib as ctx
import datetime as dt
import logging

import zmq
import zmq.asyncio

from . import types as pymine_types
from .registry import client as pymine_reg_client

_log = logging.getLogger(__name__)


async def process_client_request(client_sock, pub_sock, client_db):
    """
    process a client request
    """
    request = await client_sock.recv_serialized(
        pymine_types.ClientRequest.zmq_deserialize
    )

    client_id = pymine_reg_client.get_client_handler(request.type)(
        request, client_db, pub_sock
    )

    await client_sock.send_serialized(
        client_db.get(client_id),
        pymine_types.Client.zmq_serialize,
    )


class ClientDBMemory:
    """
    Client DB in memory implementation
    """

    def __init__(self):
        self._db = {}

    def upsert(self, client_id, **kwargs):
        """
        creates or updates a client in the database
        """
        if client_id in self._db:
            return self.update_attributes(client_id, **kwargs)

        self._db[client_id] = pymine_types.Client(
            client_id=client_id, **kwargs
        )
        now = dt.datetime.now(dt.timezone.utc)
        self._db[client_id].domain = "client.connection.pymine.de"
        self._db[client_id].first_seen = now
        self._db[client_id].last_seen = now
        return self.get(client_id)

    def update_attributes(self, client_id, **kwargs):
        """
        update attributes
        """
        if client_id not in self._db:
            raise KeyError(f"client_id ({client_id!r}) not found!")
        for key, value in kwargs.items():
            setattr(self._db[client_id], key, value)
        self._db[client_id].last_seen = dt.datetime.now(dt.timezone.utc)
        return self.get(client_id)

    def get(self, client_id):
        """
        returns a client object
        """
        return self._db[client_id]


async def client_service(  # pylint: disable=too-many-arguments
    addresses,
    publishers,
    zmq_context=None,
    event=None,
    client_db=None,
    poll_timeout=3000,
):
    """
    starts the client service

        * bind to address as zmq resp
        * bind to publisher addresses as zmq pub
    """
    if not zmq_context:
        zmq_context = zmq.asyncio.Context()

    if not event:
        event = asyncio.Event()

    if not client_db:
        client_db = ClientDBMemory()

    client_sock = zmq_context.socket(zmq.REP)
    pub_sock = zmq_context.socket(zmq.PUB)
    with ctx.ExitStack() as zmq_socks:
        for address in addresses:
            zmq_socks.enter_context(client_sock.bind(address))

        for publisher in publishers:
            zmq_socks.enter_context(pub_sock.bind(publisher))

        while not event.is_set():
            poll_event = await client_sock.poll(poll_timeout)
            _log.debug("poll event: %s", poll_event)
            if zmq.POLLIN == poll_event:
                await process_client_request(client_sock, pub_sock, client_db)


@pymine_reg_client.register_client_handler("update_client_state")
def _update_client_state(request, client_db, pub_sock):
    """
    update client state
    """
    _log.debug("update client state %r (%s)", request.state, request.client_id)
    client_id = request.client_id
    client = client_db.update_attributes(
        client_id,
        state=request.state,
        last_seen=dt.datetime.now(dt.timezone.utc),
    )
    pub_sock.send_serialized(
        [client.topic, client],
        pymine_types.Client.zmq_serialize,
    )
    return client_id


@pymine_reg_client.register_client_handler("update_client_name")
def _update_client_name(request, client_db, _):
    """
    update client state
    """
    _log.debug("update client name %r (%s)", request.name, request.client_id)
    client_id = request.client_id
    client_db.update_attributes(
        client_id,
        name=request.name,
    )
    return client_id


@pymine_reg_client.register_client_handler("disconnect_client")
def _disconnect_client(request, client_db, pub_sock):
    """
    disconnect client
    """
    _log.debug("disconnect client %s", request.client_id)
    client_id = request.client_id
    client_db.update_attributes(
        client_id,
        state="disconnect",
    )
    client = client_db.get(client_id)
    pub_sock.send_serialized(
        [client.topic, client],
        pymine_types.Client.zmq_serialize,
    )
    return client_id


@pymine_reg_client.register_client_handler("get_client")
def _get_client(request, _, __):
    """
    get client
    """
    _log.debug("get client %s", request.client_id)
    client_id = request.client_id
    return client_id


@pymine_reg_client.register_client_handler("register_client")
def _register_client(request, client_db, _):
    _log.debug("register client %s", request.client_id)
    client_id = request.client_id
    if client_id != request.client.client_id:
        raise ValueError("client_id and client.client_id don't match")
    client_db.upsert(**request.client.dict())
    return client_id


class ClientSocket:
    """
    implement client operations
    """

    def __init__(self, zmq_context=None):
        if not zmq_context:
            zmq_context = zmq.asyncio.Context()
        self._sock = zmq_context.socket(zmq.REQ)
        self.connected = False

    @ctx.contextmanager
    def connect(self, address):
        """
        connect to client service
        """
        with self._sock.connect(address):
            self.connected = True
            try:
                yield self
            finally:
                self.connected = False

    async def _request(self, **kwargs):
        if not self.connected:
            raise RuntimeError("not connected")

        await self._sock.send_serialized(
            pymine_types.ClientRequest(**kwargs),
            pymine_types.ClientRequest.zmq_serialize,
        )

        return await self._sock.recv_serialized(
            pymine_types.Client.zmq_deserialize,
        )

    async def disconnect(self, client_id):
        """
        register client on client_service
        """
        return await self._request(
            type="disconnect_client",
            client_id=client_id,
        )

    async def register(self, client):
        """
        register client on client_service
        """
        return await self._request(
            type="register_client",
            client_id=client.client_id,
            client=client,
        )

    async def get(self, client_id):
        """
        get client from client_service
        """
        return await self._request(
            type="get_client",
            client_id=client_id,
        )

    async def update_name(self, client_id, name):
        """
        get client from client_service
        """
        return await self._request(
            type="update_client_name",
            client_id=client_id,
            name=name,
        )

    async def update_state(self, client_id, state):
        """
        get client from client_service
        """
        return await self._request(
            type="update_client_state",
            client_id=client_id,
            state=state,
        )


# def handle_handshake(packet, client_id, server):
#     """
#     handle handshake request from client
#     """
#     with server["client_registry"](client_id) as client:
#         packet["handshake"], read_length = pymine_packet.decode_packet(
#             packet["packet_id"],
#             packet["data"],
#             state=client["state"],
#         )
#         _log.debug(
#             "Handle handshake %r remaining bytes %r from %r",
#             packet,
#             packet["data"][read_length:],
#             client["address"],
#         )
#         client["state"] = pymine_packet.ClientStates(
#             packet["handshake"]["next_state"]
#         )
#         _log.debug(
#             "client %r has new state %r", client["address"], client["state"]
#         )
#
#
# def handle_login_start(packet, client_id, server):
#     """
#     handle login start packet from the client
#     """
#     with server["client_registry"](client_id) as client:
#         packet["login_start"], read_length = pymine_packet.decode_packet(
#             packet["packet_id"],
#             packet["data"],
#             state=client["state"],
#         )
#         _log.debug(
#             "Handle login start %r remaining bytes %r from %r",
#             packet,
#             packet["data"][read_length:],
#             client["address"],
#         )
#         client["username"] = packet["login_start"]["username"]
#         data = pymine_packet.encode_packet(
#             0x02,
#             {
#                 "username": client["username"],
#                 "uuid": client["client_id"],
#             },
#             state=client["state"],
#         )
#         _log.debug("send packet data %r to client %r", data, client["address"])
#         client["send"](data)
#         client["state"] = pymine_packet.ClientStates.PLAY
#         # send join_game
#         data = pymine_packet.encode_packet(
#             0x24,
#             {
#                 "entity_id": client["entity_id"],
#                 "is_hardcore": False,
#                 "gamemode": 0,
#                 "previous_gamemode": -1,
#                 "world_names": ["minecraft:overworld"],
#                 "dimension_codec": None,
#                 "dimension": None,
#                 "world_name": "minecraft:overworld",
#                 "hashed_seed": 1234,
#                 "max_players": 10,
#                 "view_distance": 16,
#                 "reduced_debug_info": False,
#                 "enable_respawn_screen": True,
#                 "is_debug": False,
#                 "is_flat": True,
#             },
#             state=client["state"],
#         )
#         client["send"](data)
#         # send plugin message
#         data = pymine_packet.encode_packet(
#             0x17,
#             {
#                 "channel": "minecraft:brand",
#                 "data": pymine_packet.encode_string("pymine", max_length=16),
#             },
#             state=client["state"],
#         )
#         client["send"](data)
#         # send held item change
#         data = pymine_packet.encode_packet(
#             0x3F,
#             {
#                 "slot": 0,
#             },
#             state=client["state"],
#         )
#         client["send"](data)
#         # # send declare recipes
#         data = pymine_packet.encode_packet(
#             0x5A,
#             {
#                 "recipes": [
#                     {
#                         "type": "crafting_special_mapextending",
#                         "recipe_id": "0",
#                     }
#                 ]
#             },
#             state=client["state"],
#         )
#         client["send"](data)
#         # # send tags
#         data = pymine_packet.encode_packet(
#             0x5B,
#             {
#                 "block_tags": {
#                     "tags": [
#                         {"name": "infiniburn_end", "entries": [7]},
#                         {"name": "acacia_logs", "entries": [7]},
#                         {"name": "anvil", "entries": [7]},
#                         {"name": "bamboo_plantable_on", "entries": [7]},
#                         {"name": "banners", "entries": [7]},
#                         {"name": "base_stone_nether", "entries": [7]},
#                         {"name": "base_stone_nether", "entries": [7]},
#                         {"name": "base_stone_overworld", "entries": [7]},
#                         {"name": "beacon_base_blocks", "entries": [7]},
#                         {"name": "beds", "entries": [7]},
#                         {"name": "beehives", "entries": [7]},
#                         {"name": "bee_growables", "entries": [7]},
#                         {"name": "birch_logs", "entries": [7]},
#                         {"name": "buttons", "entries": [7]},
#                         {"name": "campfires", "entries": [7]},
#                         {"name": "carpets", "entries": [7]},
#                         {"name": "climbable", "entries": [7]},
#                         {"name": "corals", "entries": [7]},
#                         {"name": "coral_blocks", "entries": [7]},
#                         {"name": "coral_plants", "entries": [7]},
#                         {"name": "crimson_stems", "entries": [7]},
#                         {"name": "crops", "entries": [7]},
#                         {"name": "dark_oak_logs", "entries": [7]},
#                         {"name": "doors", "entries": [7]},
#                         {"name": "dragon_immune", "entries": [7]},
#                         {"name": "enderman_holdable", "entries": [7]},
#                         {"name": "fence_gates", "entries": [7]},
#                         {"name": "fences", "entries": [7]},
#                         {"name": "fire", "entries": [7]},
#                         {"name": "flowers", "entries": [7]},
#                         {"name": "flower_pots", "entries": [7]},
#                         {"name": "gold_ores", "entries": [7]},
#                         {"name": "guarded_by_piglins", "entries": [7]},
#                         {"name": "hoglin_repellents", "entries": [7]},
#                         {"name": "ice", "entries": [7]},
#                         {"name": "impermeable", "entries": [7]},
#                         {"name": "infiniburn_end", "entries": [7]},
#                         {"name": "infiniburn_nether", "entries": [7]},
#                         {"name": "infiniburn_overworld", "entries": [7]},
#                         {"name": "jungle_logs", "entries": [7]},
#                         {"name": "leaves", "entries": [7]},
#                         {"name": "logs", "entries": [7]},
#                         {"name": "logs_that_burn", "entries": [7]},
#                         {"name": "mushroom_grow_block", "entries": [7]},
#                         {"name": "non_flammable_wood", "entries": [7]},
#                         {"name": "nylium", "entries": [7]},
#                         {"name": "oak_logs", "entries": [7]},
#                         {"name": "piglin_repellents", "entries": [7]},
#                         {"name": "planks", "entries": [7]},
#                         {"name": "portals", "entries": [7]},
#                         {"name": "pressure_plates", "entries": [7]},
#                         {
#                             "name": "prevent_mob_spawning_inside",
#                             "entries": [7],
#                         },
#                         {"name": "rails", "entries": [7]},
#                         {"name": "sand", "entries": [7]},
#                         {"name": "saplings", "entries": [7]},
#                         {"name": "shulker_boxes", "entries": [7]},
#                         {"name": "signs", "entries": [7]},
#                         {"name": "slabs", "entries": [7]},
#                         {"name": "small_flowers", "entries": [7]},
#                         {"name": "soul_fire_base_blocks", "entries": [7]},
#                         {"name": "soul_speed_blocks", "entries": [7]},
#                         {"name": "spruce_logs", "entries": [7]},
#                         {"name": "stairs", "entries": [7]},
#                         {"name": "standing_signs", "entries": [7]},
#                         {"name": "stone_bricks", "entries": [7]},
#                         {"name": "stone_pressure_plates", "entries": [7]},
#                         {"name": "strider_warm_blocks", "entries": [7]},
#                         {"name": "tall_flowers", "entries": [7]},
#                         {"name": "trapdoors", "entries": [7]},
#                         {"name": "underwater_bonemeals", "entries": [7]},
#                         {"name": "unstable_bottom_center", "entries": [7]},
#                         {"name": "valid_spawn", "entries": [7]},
#                         {"name": "walls", "entries": [7]},
#                         {"name": "wall_corals", "entries": [7]},
#                         {"name": "wall_post_override", "entries": [7]},
#                         {"name": "wall_signs", "entries": [7]},
#                         {"name": "warped_stems", "entries": [7]},
#                         {"name": "wart_blocks", "entries": [7]},
#                         {"name": "wither_immune", "entries": [7]},
#                         {"name": "wither_summon_base_blocks", "entries": [7]},
#                         {"name": "wooden_buttons", "entries": [7]},
#                         {"name": "wooden_doors", "entries": [7]},
#                         {"name": "wooden_fences", "entries": [7]},
#                         {"name": "wooden_pressure_plates", "entries": [7]},
#                         {"name": "wooden_slabs", "entries": [7]},
#                         {"name": "wooden_stairs", "entries": [7]},
#                         {"name": "wooden_trapdoors", "entries": [7]},
#                         {"name": "wool", "entries": [7]},
#                     ]
#                 },
#                 "item_tags": {
#                     "tags": [
#                         {"name": "acacia_logs", "entries": [333]},
#                         {"name": "anvil", "entries": [333]},
#                         {"name": "arrows", "entries": [333]},
#                         {"name": "banners", "entries": [333]},
#                         {"name": "beacon_payment_items", "entries": [333]},
#                         {"name": "beds", "entries": [333]},
#                         {"name": "birch_logs", "entries": [333]},
#                         {"name": "boats", "entries": [333]},
#                         {"name": "buttons", "entries": [333]},
#                         {"name": "carpets", "entries": [333]},
#                         {"name": "coals", "entries": [333]},
#                         {"name": "creeper_drop_music_discs", "entries": [333]},
#                         {"name": "crimson_stems", "entries": [333]},
#                         {"name": "dark_oak_logs", "entries": [333]},
#                         {"name": "doors", "entries": [333]},
#                         {"name": "fences", "entries": [333]},
#                         {"name": "fishes", "entries": [333]},
#                         {"name": "flowers", "entries": [333]},
#                         {"name": "gold_ores", "entries": [333]},
#                         {"name": "jungle_logs", "entries": [333]},
#                         {"name": "leaves", "entries": [333]},
#                         {"name": "lectern_books", "entries": [333]},
#                         {"name": "logs", "entries": [333]},
#                         {"name": "logs_that_burn", "entries": [333]},
#                         {"name": "music_discs", "entries": [333]},
#                         {"name": "non_flammable_wood", "entries": [333]},
#                         {"name": "oak_logs", "entries": [333]},
#                         {"name": "piglin_loved", "entries": [333]},
#                         {"name": "piglin_repellents", "entries": [333]},
#                         {"name": "planks", "entries": [333]},
#                         {"name": "rails", "entries": [333]},
#                         {"name": "sand", "entries": [333]},
#                         {"name": "saplings", "entries": [333]},
#                         {"name": "signs", "entries": [333]},
#                         {"name": "slabs", "entries": [333]},
#                         {"name": "small_flowers", "entries": [333]},
#                         {"name": "soul_fire_base_blocks", "entries": [333]},
#                         {"name": "spruce_logs", "entries": [333]},
#                         {"name": "stairs", "entries": [333]},
#                         {"name": "stone_bricks", "entries": [333]},
#                         {"name": "stone_crafting_materials", "entries": [333]},
#                         {"name": "stone_tool_materials", "entries": [333]},
#                         {"name": "tall_flowers", "entries": [333]},
#                         {"name": "trapdoors", "entries": [333]},
#                         {"name": "walls", "entries": [333]},
#                         {"name": "warped_stems", "entries": [333]},
#                         {"name": "wooden_buttons", "entries": [333]},
#                         {"name": "wooden_doors", "entries": [333]},
#                         {"name": "wooden_fences", "entries": [333]},
#                         {"name": "wooden_pressure_plates", "entries": [333]},
#                         {"name": "wooden_slabs", "entries": [333]},
#                         {"name": "wooden_stairs", "entries": [333]},
#                         {"name": "wooden_trapdoors", "entries": [333]},
#                         {"name": "wool", "entries": [333]},
#                     ]
#                 },
#                 "fluid_tags": {
#                     "tags": [
#                         {"name": "lava", "entries": [11]},
#                         {"name": "water", "entries": [9]},
#                     ]
#                 },
#                 "entity_tags": {
#                     "tags": [
#                         {"name": "arrows", "entries": [262]},
#                         {"name": "beehive_inhabitors", "entries": [262]},
#                         {"name": "impact_projectiles", "entries": [262]},
#                         {"name": "raiders", "entries": [262]},
#                         {"name": "skeletons", "entries": [262]},
#                     ]
#                 },
#             },
#             state=client["state"],
#         )
#         client["send"](data)
#         # send entity status
#         data = pymine_packet.encode_packet(
#             0x1A,
#             {
#                 "entity_id": client["entity_id"],
#                 "entity_status": 28,  # op permission level 4
#             },
#             state=client["state"],
#         )
#         client["send"](data)
#         # send declare commands
#         # data = pymine_packet.encode_packet(
#         #     0x10,
#         #     {
#         #         "nodes": [{
#         #
#         #         }]
#         #     }
#         # )
#         # send Unlock recipes
#         # https://wiki.vg/Protocol#Unlock_Recipes
#         # send play position and look
#         # https://wiki.vg/Protocol#Player_Position_And_Look_.28clientbound.29
#         # send add player
#         # https://wiki.vg/Protocol#Player_Info
#         # send update latency
#         # https://wiki.vg/Protocol#Player_Info
#         # send update view position
#         # https://wiki.vg/Protocol#Update_View_Position
#         # send Update Light
#         # https://wiki.vg/Protocol#Update_Light
#         # send Chunk Data
#         # https://wiki.vg/Protocol#Chunk_Data
#         # send World Border
#         # https://wiki.vg/Protocol#World_Border
#         # send set spawnpoint
#         # https://wiki.vg/Protocol#Spawn_Position
#         # send player position and view
#         # https://wiki.vg/Protocol#Player_Position_And_Look_.28clientbound.29
#
#
# def handle_plugin(*_):
#     """
#     handle plugin run
#     """
#
#
# HANDLER_REGISTRY = {
#     #     (0x00, pymine_packet.ClientStates.HANDSHAKE): handle_handshake,
#     #     (0x00, pymine_packet.ClientStates.LOGIN): handle_login_start,
#     #     (0x0B, pymine_packet.ClientStates.PLAY): handle_plugin,
# }
#
#
# async def handler(  # pylint: disable=dangerous-default-value
#     client_id, input_queue, server, handler_registry=HANDLER_REGISTRY
# ):
#     """
#     handle input packages
#     """
#     while True:
#         packet = await input_queue.get()
#         packet_id = packet["packet_id"]
#
#         with server["client_registry"](client_id) as client:
#             addr = client["address"]
#             state = client["state"]
#             handler_key = packet_id, state
#
#         if handler_key in handler_registry:
#             handler_registry[handler_key](packet, client_id, server)
#         else:
#             _log.error(
#                 "no handler for packet_id %r and state %r found from %r",
#                 packet_id,
#                 state,
#                 addr,
#             )
