"""
command line interface of pymine
"""
import asyncio
import logging

import click

from . import client as pymine_client
from . import package_handler as pymine_package_handler
from . import server as pymine_server

_log = logging.getLogger(__name__)


@click.group()
@click.option("--verbose", is_flag=True, default=False)
@click.option("--debug", is_flag=True, default=False)
@click.pass_context
def main(ctx, verbose, debug):
    """
    base click group to configure base functionality
    """
    ctx.ensure_object(dict)
    logging.basicConfig()
    if debug:
        logging.getLogger().setLevel(logging.DEBUG)
        _log.debug("set log level to debug")
        ctx.obj["debug"] = True
    elif verbose:
        logging.getLogger().setLevel(logging.INFO)
        _log.info("set log level to info")
    else:
        logging.getLogger().setLevel(logging.WARNING)
        _log.warning("set log level to warning")


@main.group()
def server():
    """
    command group for server components
    """


# pylint: disable=too-many-arguments
@server.command()
@click.option(
    "--host",
    "-H",
    default=["[::]"],
    multiple=True,
    help="listen on this address",
)
@click.option(
    "--port",
    "-p",
    type=click.IntRange(1024, 65535),
    default=25565,
    help="listen on this port",
)
@click.option(
    "--publisher",
    default=["tcp://127.0.0.1:25568", "tcp://127.0.0.1:25567"],
    multiple=True,
    help="address of publisher",
)
@click.option(
    "--package-handler-address",
    "-P",
    default=["tcp://127.0.0.1:25566"],
    multiple=True,
    help="address of package handler",
)
@click.option(
    "--client-service",
    "-C",
    default="tcp://127.0.0.1:25569",
    help="address of client service",
)
@click.pass_context
def connection(
    ctx, host, port, package_handler_address, publisher, client_service
):
    """
    start pymine server
    """
    hosts = []
    for hostname in host:
        if ":" in hostname:
            hostname = hostname.strip("]").strip("[")
        hosts.append(hostname)
    asyncio.run(
        pymine_server.connection_service(
            hosts, port, publisher, package_handler_address, client_service
        ),
        debug=ctx.obj.get("debug", False),
    )


@server.command()
@click.option(
    "--host",
    "-H",
    default=["tcp://127.0.0.1:25569"],
    multiple=True,
    help="listen on this address",
)
@click.option(
    "--publisher",
    default=["tcp://127.0.0.1:25568"],
    multiple=True,
    help="listen on this address",
)
@click.pass_context
def client(ctx, host, publisher):
    """
    start a client service
    """
    asyncio.run(
        pymine_client.client_service(host, publisher),
        debug=ctx.obj.get("debug", False),
    )


@server.command()
@click.option(
    "--host",
    "-H",
    default=["tcp://127.0.0.1:25566"],
    multiple=True,
    help="listen on this address",
)
@click.option(
    "--publisher",
    default=["tcp://127.0.0.1:25567"],
    multiple=True,
    help="listen on this address",
)
@click.option(
    "--client-service",
    "-C",
    default="tcp://127.0.0.1:25569",
    help="address of client service",
)
@click.pass_context
def package_handler(ctx, host, publisher, client_service):
    """
    start a package handler
    """
    asyncio.run(
        pymine_package_handler.package_handler(
            host, publisher, client_service
        ),
        debug=ctx.obj.get("debug", False),
    )
