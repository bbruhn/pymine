"""
helper functions to decode the packet
"""
from . import types as _
from .registry import packet as pymine_reg_packet

pymine_reg_packet.register_packet(
    0x00,
    "handshake",
    [
        ("protocol_version", "varint"),
        {
            "name": "server_address",
            "attr_type": "string",
            "kwargs": {"max_length": 255},
        },
        ("server_port", "uint16"),
        {
            "name": "next_state",
            "attr_type": "enum",
            "kwargs": {
                "type_def": "varint",
                "values": {0x01: "status", 0x02: "login"},
            },
        },
    ],
)


pymine_reg_packet.register_packet(
    0x00,
    "login",
    [
        {
            "name": "name",
            "attr_type": "string",
            "kwargs": {"max_length": 16},
        },
    ],
)


pymine_reg_packet.register_packet(
    0x02,
    "login",
    [
        {
            "name": "uuid",
            "attr_type": "uuid",
        },
        {
            "name": "username",
            "attr_type": "string",
            "kwargs": {"max_length": 16},
        },
    ],
)


pymine_reg_packet.register_packet(
    0x05,
    "play",
    [
        {
            "name": "locale",
            "attr_type": "string",
            "kwargs": {
                "max_length": 16,
            },
        },
        {
            "name": "view_distance",
            "attr_type": "int8",
        },
        {
            "name": "chat_mode",
            "attr_type": "enum",
            "kwargs": {
                "type_def": "varint",
                "values": {
                    0: "enabled",
                    1: "commands_only",
                    2: "hidden",
                },
            },
        },
        {
            "name": "chat_colors",
            "attr_type": "boolean",
        },
        {
            "name": "display_skin_parts",
            "attr_type": "bitmask",
            "kwargs": {
                "type_def": "uint8",
                "values": {
                    0x01: "cape",
                    0x02: "jacket",
                    0x04: "left_sleeve",
                    0x08: "right_sleeve",
                    0x10: "left_pants_leg",
                    0x20: "right_pants_leg",
                    0x40: "hat",
                },
            },
        },
        {
            "name": "main_hand",
            "attr_type": "enum",
            "kwargs": {
                "type_def": "varint",
                "values": {
                    0: "left",
                    1: "right",
                },
            },
        },
    ],
)


pymine_reg_packet.register_packet(
    0x0B,
    "play",
    [
        {
            "name": "channel",
            "attr_type": "string",
        },
        {
            "name": "data",
            "attr_type": "raw",
        },
    ],
)


pymine_reg_packet.register_packet(
    0x17,
    "play",
    [
        {
            "name": "channel",
            "attr_type": "string",
        },
        {
            "name": "data",
            "attr_type": "raw",
        },
    ],
)


pymine_reg_packet.register_packet(
    0x24,
    "play",
    [
        {
            "name": "entity_id",
            "attr_type": "int32",
        },
        {
            "name": "is_hardcore",
            "attr_type": "boolean",
        },
        {
            "name": "gamemode",
            "attr_type": "uint8",
        },
        {
            "name": "previous_gamemode",
            "attr_type": "int8",
        },
        {
            "name": "world_names",
            "attr_type": "array",
            "kwargs": {
                "inner_type": "string",
                "length_type": "varint",
            },
        },
        {
            "name": "dimension_codec",
            "attr_type": "dimension codec",
        },
        {
            "name": "dimension",
            "attr_type": "dimension",
        },
        {
            "name": "world_name",
            "attr_type": "string",
        },
        {
            "name": "hashed_seed",
            "attr_type": "int64",
        },
        {
            "name": "max_players",
            "attr_type": "varint",
        },
        {
            "name": "view_distance",
            "attr_type": "varint",
            "kwargs": {
                "minimum": 2,
                "maximum": 32,
            },
        },
        {
            "name": "reduced_debug_info",
            "attr_type": "boolean",
        },
        {
            "name": "enable_respawn_screen",
            "attr_type": "boolean",
        },
        {
            "name": "is_debug",
            "attr_type": "boolean",
        },
        {
            "name": "is_flat",
            "attr_type": "boolean",
        },
    ],
)


pymine_reg_packet.register_packet(
    0x3F,
    "play",
    [
        {
            "name": "slot",
            "attr_type": "int8",
        },
    ],
)
# class UncompressedPacket:
#     def decode(self, data):
#         length = 0
#         packet_length, _length = pymine_types.Varint().decode(data[length:])
#         length += _length
#         packet_id, _length = pymine_types.Varint().decode(data[length:])
#         length += _length
#         return (
#             pymine_types.UncompressedPacket(
#                 length=packet_length,
#                 packet_id=packet_id,
#             ),
#             length,
#         )
#
#
# class MetaPacket(type):
#     """
#     Meta class for a packet
#     """
#     def __init__(cls, name, bases, dct):
#         super().__init__(cls, name, bases, dct)
#         fields = []
#         for base in bases:
#             fields.extend(base._fields)
#         fields.extend(cls._fields)
#         cls._fields = fields
#
#
# class BasePacket(metaclass=MetaPacket):
#     """
#     BasePacket
#     """
#     _fields = [
#         ("length", pymine_types.Varint()),
#     ]
#
#     def __init__(self, **kwargs):
#         self.raw_data = None
#         self.original_data = None
#         for name, value in self._fields:
#             setattr(self, name, None)
#         field_names = set(f[0] for f in self._fields)
#         for name, value in kwargs.items():
#             if name not in field_names:
#                 raise ValueError("Invalid fields: {name}")
#             self.name = value
#
#     def decode(self, data):
#         """
#         generate Base Packet from bytes
#         """
#         instance = cls()
#         instance.original_data = obj
#         length = 0
#         for name, value in cls._fields:
#             val, _length = value.decode(obj[length:])
#             setattr(instance, name, val)
#             length += _length
#         instance.raw_data = obj[length:]
#
#         return instance
#
#     def encode(self, obj):
#         """
#         generate bytes from Base Packet
#         """
#         result = b""
#         for name, value in self._fields:
#             result += value.encode(getattr(self, name))
#
#         if self.raw_data:
#             result += self.raw_data
#         return result
#
# class UncompressedPacket(BasePacket):
#     """
#     definition of a uncompressed packet
#     """
#     _fields = [
#         ("packet_id", pymine_types.Varint()),
#     ]
#
#
# TAGS = ft.partial(
#     encode_obj,
#     packet={
#         "tags": {
#             "encode": ft.partial(
#                 encode_array,
#                 inner_encode=ft.partial(
#                     encode_obj,
#                     packet={
#                         "name": {
#                             "encode": ft.partial(
#                                 encode_string, max_length=32767
#                             )
#                         },
#                         "entries": {
#                             "encode": ft.partial(
#                                 encode_array,
#                                 inner_encode=encode_varint,
#                                 length_encode=encode_varint,
#                             )
#                         },
#                     },
#                 ),
#                 length_encode=encode_varint,
#             )
#         },
#     },
# )
#
#
# UncompressedPacket.from_bytes(b"asdas").packet_id == 0x00
# UncompressedPacket().packet_id = 0x00
# UncompressedPacket(packet_id=0x00)
# HandshakePacket.from_bytes(b"asfasfda")
# UncompressedPacket(packet_id=0x00).to_bytest()
#
#
# PACKET_REGISTRY = {
#     (0x00, ClientStates.HANDSHAKE): {
#         "protocol_version": {"decode": decode_varint},
#         "server_address": {
#             "decode": ft.partial(decode_string, max_length=255)
#         },
#         "server_port": {"decode": decode_uint16},
#         "next_state": {"decode": decode_varint},
#     },
#     (0x00, ClientStates.LOGIN): {
#         "username": {"decode": ft.partial(decode_string, max_length=16)}
#     },
#     (0x02, ClientStates.LOGIN): {
#         "uuid": {"encode": encode_uuid},
#         "username": {"encode": ft.partial(encode_string, max_length=16)},
#     },
#     (0x24, ClientStates.PLAY): {
#         "entity_id": {"encode": encode_int32},
#         "is_hardcore": {"encode": encode_boolean},
#         "gamemode": {"encode": encode_uint8},
#         "previous_gamemode": {"encode": encode_int8},
#         "world_names": {
#             "encode": ft.partial(
#                 encode_array,
#                 inner_encode=ft.partial(encode_string, max_length=32767),
#                 length_encode=encode_varint,
#             )
#         },
#         "dimension_codec": {"encode": generate_encode_dimension_codec()},
#         "dimension": {"encode": encode_dimension},
#         "world_name": {"encode": ft.partial(encode_string, max_length=32767)},
#         "hashed_seed": {"encode": encode_int64},
#         "max_players": {"encode": encode_varint},
#         "view_distance": {
#             "encode": ft.partial(encode_varint, minimum=2, maximum=32)
#         },
#         "reduced_debug_info": {"encode": encode_boolean},
#         "enable_respawn_screen": {"encode": encode_boolean},
#         "is_debug": {"encode": encode_boolean},
#         "is_flat": {"encode": encode_boolean},
#     },
#     (0x17, ClientStates.PLAY): {
#         "channel": {"encode": ft.partial(encode_string, max_length=32767)},
#         "data": {"encode": encode_raw},
#     },
#     (0x05, ClientStates.PLAY): {
#         "locale": {"decode": ft.partial(decode_string, max_length=16)},
#         "view_distance": {"decode": decode_int8},
#         "chat_mode": {
#             "decode": ft.partial(
#                 decode_enum,
#                 decode=decode_varint,
#                 values={0: "enabled", 1: "commands_only", 2: "hidden"},
#             )
#         },
#         "chat_colors": {"decode": decode_boolean},
#         "display_skin_parts": {
#             "decode": ft.partial(
#                 decode_bitmask,
#                 decode=decode_uint8,
#                 values={
#                     0x01: "cape",
#                     0x02: "jacket",
#                     0x04: "left_sleeve",
#                     0x08: "right_sleeve",
#                     0x10: "left_pants_leg",
#                     0x20: "right_pants_leg",
#                     0x40: "hat",
#                 },
#             )
#         },
#         "main_hand": {
#             "decode": ft.partial(
#                 decode_enum,
#                 decode=decode_varint,
#                 values={0: "left", 1: "right"},
#             )
#         },
#     },
#     (0x0B, ClientStates.PLAY): {
#         "channel": {"decode": ft.partial(decode_string, max_length=32767)},
#         "data": {"decode": decode_raw},
#     },
#     (0x3F, ClientStates.PLAY): {
#         "slot": {"encode": encode_int8},
#     },
#     (0x5A, ClientStates.PLAY): {
#         "recipes": {
#             "encode": ft.partial(
#                 encode_array,
#                 inner_encode=ft.partial(
#                     encode_obj,
#                     packet={
#                         "type": {
#                             "encode": ft.partial(
#                                 encode_string, max_length=32767
#                             )
#                         },
#                         "recipe_id": {
#                             "encode": ft.partial(
#                                 encode_string, max_length=32767
#                             )
#                         },
#                         "data": {"encode": encode_raw, "optional": True},
#                     },
#                 ),
#                 length_encode=encode_varint,
#             )
#         }
#     },
#     (0x5B, ClientStates.PLAY): {
#         "block_tags": {"encode": TAGS},
#         "item_tags": {"encode": TAGS},
#         "fluid_tags": {"encode": TAGS},
#         "entity_tags": {"encode": TAGS},
#     },
#     (0x1A, ClientStates.PLAY): {
#         "entity_id": encode_int32,
#         "entity_status": encode_int8,
#     },
# }
#
#
# def encode_packet(  # pylint: disable=dangerous-default-value
#     packet_id, obj, state=ClientStates.PLAY, packet_registry=PACKET_REGISTRY
# ):
#     """
#     encode a packet with packet registry
#     """
#     result = b""
#     result += encode_varint(packet_id)
#     result += encode_obj(obj, packet_registry[(packet_id, state)])
#     result = encode_varint(len(result)) + result
#     return result
#
#
# def decode_packet(  # pylint: disable=dangerous-default-value
#     packet_id, data, state=ClientStates.PLAY, packet_registry=PACKET_REGISTRY
# ):
#     """
#     decode a packet with packet registry
#     """
#     result = {}
#     total_read = 0
#     for fieldname, decode_obj in packet_registry[(packet_id, state)].items():
#         result[fieldname], read_length = decode_obj["decode"](data)
#         data = data[read_length:]
#         total_read += read_length
#     _log.debug(
#         "decode packet %r for state %r with remaining_data %r and result %r",
#         packet_id,
#         state,
#         data,
#         result,
#     )
#     return result, total_read
