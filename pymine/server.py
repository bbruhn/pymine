"""
server connection handling
"""
import asyncio
import contextlib as ctx
import functools as ft
import logging
import socket
import uuid

import zmq
import zmq.asyncio

from . import client as pymine_client
from . import types as pymine_types
from .registry import packet as pymine_reg_packet

_log = logging.getLogger(__name__)


@ctx.asynccontextmanager
async def get_tcp_server(hosts, port, client_callback, loop=None):
    """
    starts multiple tcp socket server and register a client callback
    """
    if not loop:
        loop = asyncio.get_running_loop()

    with ctx.ExitStack() as sockets:
        async with ctx.AsyncExitStack() as servers:
            tasks = []
            for host in hosts:
                for (
                    addr_family,
                    socktype,
                    proto,
                    _,
                    sock_addr,
                ) in await loop.getaddrinfo(
                    host, port, proto=socket.IPPROTO_TCP
                ):
                    sock = sockets.enter_context(
                        socket.socket(addr_family, socktype, proto)
                    )
                    sock.bind(sock_addr)
                    server = await servers.enter_async_context(
                        await asyncio.start_server(
                            client_callback,
                            sock=sock,
                        )
                    )
                    _log.info("Listen on %r", sock_addr)
                    tasks.append(server.serve_forever())
            yield tasks


async def connection_service(  # pylint: disable=too-many-arguments
    hosts,
    port,
    announcers,
    package_handlers,
    client_service,
    loop=None,
    zmq_context=None,
):
    """
    starts the connection service

        * bind to hosts and port
        * accept new connections
        * create zmq context
    """
    if not loop:
        loop = asyncio.get_running_loop()

    if not zmq_context:
        zmq_context = zmq.asyncio.Context()

    client_callback = ft.partial(
        zmq_tcp_conn_client,
        client_service=client_service,
        announcers=announcers,
        package_handlers=package_handlers,
        zmq_context=zmq_context,
        loop=loop,
    )
    async with get_tcp_server(hosts, port, client_callback) as servers:
        # starts serve_forever on undelying tcp server
        result = await asyncio.gather(*servers)
    return result


async def zmq_tcp_conn_client_sender(  # pylint: disable=too-many-locals,too-many-arguments
    stream_writer,
    zmq_context,
    publishers,
    client_service,
    client_id,
    writer_event,
):
    """
    sends packages received by subscribe socket.
    """
    _log.info("starting writer for %s", client_id)
    client_sock = pymine_client.ClientSocket(zmq_context)
    sub_sock = zmq_context.socket(zmq.SUB)

    with ctx.ExitStack() as zmq_sockets:
        zmq_sockets.enter_context(client_sock.connect(client_service))

        client_obj = await client_sock.get(client_id)
        state_topic = client_obj.state_topic
        sub_sock.subscribe(state_topic)
        sub_sock.subscribe(client_obj.topic)

        for publisher in publishers:
            zmq_sockets.enter_context(sub_sock.connect(publisher))

        while not writer_event.is_set():
            client_obj = await client_sock.get(client_id)
            if client_obj.state_topic != state_topic:
                sub_sock.subscribe(client_obj.state_topic)
                sub_sock.unsubscribe(state_topic)
                state_topic = client_obj.state_topic

            poll_event = await sub_sock.poll(3000)
            _log.debug("poll event: %s (%s)", poll_event, client_obj.client_id)
            if zmq.POLLIN == poll_event:
                client_obj = await client_sock.get(client_id)
                msg_parts = await sub_sock.recv_multipart()
                _log.debug("msg_parts: %r (%s)", msg_parts, client_id)

                handle_topic = msg_parts[0].decode()

                if handle_topic == client_obj.topic:
                    continue

                if handle_topic == state_topic:
                    packet = pymine_types.Packet.zmq_deserialize(
                        [msg_parts[1]]
                    )
                    bytes_to_send = pymine_reg_packet.encode_packet(
                        client_obj.state, packet.packet
                    )
                    _log.debug(
                        "send packet: %s (%r)",
                        client_obj.client_id,
                        bytes_to_send,
                    )
                    stream_writer.write(bytes_to_send)
                    await stream_writer.drain()
                else:
                    raise RuntimeError("unknown topic: %s")

        _log.info("Close the connection %r", client_obj)
        stream_writer.close()


async def zmq_tcp_conn_client(  # pylint: disable=too-many-arguments,too-many-locals
    stream_reader,
    stream_writer,
    client_service,
    announcers,
    package_handlers,
    zmq_context,
    loop,
):
    """
    handles a tcp connection from a client.

        * subscribes to announcers
        * connect to package handlers
    """
    addr = stream_writer.get_extra_info("peername")
    if ":" in addr[0]:
        str_addr = f"[{addr[0]}]:{addr[1]}"
    else:
        str_addr = f"{addr[0]}:{addr[1]}"
    _log.debug("addr: %r", addr)
    client_obj = pymine_types.Client(
        address=str_addr,
        client_id=uuid.uuid5(uuid.NAMESPACE_URL, str_addr),
        state="handshake",
    )
    with ctx.ExitStack() as zmq_sockets:
        client_sock = pymine_client.ClientSocket(zmq_context)
        zmq_sockets.enter_context(client_sock.connect(client_service))

        client_obj = await client_sock.register(client_obj)

        push_sock = zmq_context.socket(zmq.PUSH)
        for package_handler in package_handlers:
            zmq_sockets.enter_context(push_sock.connect(package_handler))

        writer_event = asyncio.Event()
        writer_task = loop.create_task(
            zmq_tcp_conn_client_sender(
                stream_writer,
                zmq_context,
                announcers,
                client_service,
                client_obj.client_id,
                writer_event,
            )
        )

        data = b""
        while True:
            if len(data) < 5:
                data += await stream_reader.read(1024)
                if not data:
                    break
            _log.debug("Received data %r from %r", data, addr)

            varint = pymine_types.Varint()
            packet_length, length_read = varint.decode(data)
            full_packet_length = packet_length + length_read

            _log.debug(
                "Expected packet_length %r already read %r varint size %r",
                packet_length,
                len(data),
                length_read,
            )

            if full_packet_length > len(data):
                data += await stream_reader.readexactly(
                    full_packet_length - len(data)
                )

            packet_data = data[length_read:full_packet_length]
            data = data[full_packet_length:]

            # update current client_obj
            client_obj = await client_sock.get(client_obj.client_id)

            decoded_packet = pymine_reg_packet.decode_packet(
                client_obj.state, packet_data
            )

            # generate base packet
            packet = pymine_types.Packet(
                length=packet_length,
                client_id=client_obj.client_id,
                client_state=client_obj.state,
                packet=decoded_packet,
            )

            _log.debug(
                "Received packet %r extracted from %r"
                " and unparsed data %r from %r",
                packet,
                packet_data,
                data,
                addr,
            )

            push_sock.send_serialized(
                packet, pymine_types.Packet.zmq_serialize
            )

            if (
                decoded_packet["identifier"] == 0x00
                and client_obj.state == "handshake"
            ):
                await client_sock.update_state(
                    client_obj.client_id,
                    decoded_packet["next_state"],
                )

        writer_event.set()
        await writer_task

        await client_sock.disconnect(client_obj.client_id)
